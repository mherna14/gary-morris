<?php
ob_start();
/*
This is where you can drop your custom functions or
just edit things like thumbnail sizes, header images,
sidebars, comments, ect.
*/

/*********************
INCLUDE NEEDED FILES
*********************/

/*
library/joints.php
	- head cleanup (remove rsd, uri links, junk css, ect)
	- enqueueing scripts & styles
	- theme support functions
	- custom menu output & fallbacks
	- related post function
	- page-navi function
	- removing <p> from around images
	- customizing the post excerpt
	- custom google+ integration
	- adding custom fields to user profiles
*/
require_once(get_template_directory().'/library/joints.php'); // if you remove this, Joints will break
/*
library/custom-post-type.php
	- an example custom post type
	- example custom taxonomy (like categories)
	- example custom taxonomy (like tags)
*/
require_once(get_template_directory().'/library/custom-post-type-site.php'); //
require_once(get_template_directory().'/library/custom-post-type-launch.php'); //
//require_once(get_template_directory().'/library/custom-post-type-book.php'); //
//require_once(get_template_directory().'/library/custom-post-type-help_link.php'); //
//require_once(get_template_directory().'/library/custom-post-type-questions.php'); //
//require_once(get_template_directory().'/library/custom-post-type.php'); // you can disable this if you like
/*
library/admin.php
	- removing some default WordPress dashboard widgets
	- an example custom dashboard widget
	- adding custom login css
	- changing text in footer of admin
*/
// require_once(get_template_directory().'/library/admin.php'); // this comes turned off by default
/*
library/translation/translation.php
	- adding support for other languages
*/
// require_once(get_template_directory().'/library/translation/translation.php'); // this comes turned off by default


//require_once(get_template_directory().'/cmb.php'); // Custom metaboxes pluggin from https://github.com/WebDevStudios/Custom-Metaboxes-and-Fields-for-WordPress

//require_once(get_template_directory().'/library/custom-user-profile-photo/3five_cupp.php'); // User Profile picture pluggin


/*********************
MENUS & NAVIGATION
*********************/
// registering wp3+ menus
register_nav_menus(
	array(
		'main-nav' => __( 'The Main Menu' ),   // main nav in header
		'footer-links' => __( 'Footer Links' ) // secondary nav in footer
	)
);

// the main menu
function joints_main_nav() {
	// display the wp3 menu if available
    wp_nav_menu(array(
    	'container' => false,                           // remove nav container
    	'container_class' => '',           // class of container (should you choose to use it)
    	'menu' => __( 'The Main Menu', 'jointstheme' ),  // nav name
    	'menu_class' => '',         // adding custom nav class
    	'theme_location' => 'main-nav',                 // where it's located in the theme
    	'before' => '',                                 // before the menu
        'after' => '',                                  // after the menu
        'link_before' => '',                            // before each link
        'link_after' => '',                             // after each link
    	'fallback_cb' => 'joints_main_nav_fallback'      // fallback function
	));
} /* end joints main nav */

// the footer menu (should you choose to use one)
function joints_footer_links() {
	// display the wp3 menu if available
    wp_nav_menu(array(
    	'container' => '',                              // remove nav container
    	'container_class' => 'footer-links clearfix',   // class of container (should you choose to use it)
    	'menu' => __( 'Footer Links', 'jointstheme' ),   // nav name
    	'menu_class' => 'sub-nav',      // adding custom nav class
    	'theme_location' => 'footer-links',             // where it's located in the theme
    	'before' => '',                                 // before the menu
        'after' => '',                                  // after the menu
        'link_before' => '',                            // before each link
        'link_after' => '',                             // after each link
        'depth' => 0,                                   // limit the depth of the nav
    	'fallback_cb' => 'joints_footer_links_fallback'  // fallback function
	));
} /* end joints footer link */

// this is the fallback for header menu
function joints_main_nav_fallback() {
	wp_page_menu( array(
		'show_home' => true,
    	'menu_class' => '',      // adding custom nav class
		'include'     => '',
		'exclude'     => '',
		'echo'        => true,
        'link_before' => '',                            // before each link
        'link_after' => ''                             // after each link
	) );
}

// this is the fallback for footer menu
function joints_footer_links_fallback() {
	/* you can put a default here if you like */
}

/*********************
Breakcrumbs
*********************/

function the_breadcrumb() {
		global $post;
	    echo '<ul class="breadcrumbs">';
	    echo '<li><a  href="http://library.stedwards.edu/">Munday Library</a></li>';
        if (!is_home()) {
			echo '<li><a  href="'.home_url().'">'.get_bloginfo('name').'</a></li>';
            if (is_category()) {
                echo '<li class="current">'.single_cat_title().'</li>';
                    //the_category(' </li> ');


            } elseif (is_page_template('guides.php')) {
            	echo '<li><a  href="/Guides">Guides</a></li>';

			} elseif (is_singular('launch')) {
				echo '<li>'.single_term_title("", false).'</li>';
				echo '<li class="current">launch '.get_the_title().'</li>';

			}
			elseif (is_singular('site')) {
				if($post->post_parent){
					$anc = get_post_ancestors( $post->ID );
					$title = get_the_title();
					$output = '';
					foreach ( $anc as $ancestor ) {
						$output = '<li><a href="'.get_page_link($ancestor).'">'.get_the_title($ancestor).'</a></li>'.$output;
					}
					echo $output;
					echo '<li><a href="'.get_the_permalink().'">'.$title.'</a></li>';
				} else {
					echo '<li class="current">'.get_the_title().'</li>';
				}

			}

			else if ( is_author() ){


				$author = get_user_by( 'slug', get_query_var( 'author_name' ) );
				echo '<li><a  href="'.home_url().'/profiles/">Profiles</a></li><li class="current">'.$author->display_name.'</li>';
			}



	    }
	    else{
	    	echo '<li class="current">'.get_bloginfo('name').'</li>';
	    }
	    echo '</ul>';

}

/*********************
TAGS AND CATEGORIES
*********************/

// add tag support to pages
function tags_support_all() {
	register_taxonomy_for_object_type('post_tag', 'page');
	register_taxonomy_for_object_type('category', 'page');
}

// ensure all tags are included in queries
function tags_support_query($wp_query) {
	if ($wp_query->get('tag')) $wp_query->set('post_type', 'any');
}

// tag hooks
add_action('init', 'tags_support_all');
add_action('pre_get_posts', 'tags_support_query');


/*********************
DESCRIPTIONS
*********************/

add_post_type_support( 'page', 'excerpt' );

function get_excerpt($count){
  $permalink = get_permalink($post->ID);
  $excerpt = get_the_excerpt();
  $excerpt = strip_tags($excerpt);
  $excerpt = substr($excerpt, 0, $count);
  $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
  $excerpt = $excerpt.'... <a href="'.$permalink.'">Read More</a>';
  return $excerpt;
 }

/*********************
SIDEBARS
*********************/

// Sidebars & Widgetizes Areas
function joints_register_sidebars() {
	/* register_sidebar(array(
		'id' => 'homepage',
		'name' => __('Homepage', 'jointstheme'),
		'description' => __('List of published guides displayed on the Homepage', 'jointstheme'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	)); */

	register_sidebar(array(
		'id' => 'sidebar',
		'name' => __('Sidebar Homepage', 'jointstheme'),
		'description' => __('Left side bar on Homepage', 'jointstheme'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	register_sidebar(array(
		'id' => 'sidebar-page',
		'name' => __('Sidebar Content Page', 'jointstheme'),
		'description' => __('Right side bar on contentpages', 'jointstheme'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	 register_sidebar(array(
		'id' => 'offcanvas',
		'name' => __('Offcanvas', 'jointstheme'),
		'description' => __('The offcanvas sidebar.', 'jointstheme'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	register_sidebar(array(
		'id' => 'footer-help',
		'name' => __('Need Help Footer', 'jointstheme'),
		'description' => __('The Need Help footer with icons.', 'jointstheme'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
	));

	register_sidebar(array(
		'id' => 'footer-col-1',
		'name' => __('Footer Column 1', 'jointstheme'),
		'description' => __('Footer Column 1', 'jointstheme'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	register_sidebar(array(
		'id' => 'footer-col-2',
		'name' => __('Footer Column 2', 'jointstheme'),
		'description' => __('Footer Column 2', 'jointstheme'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	register_sidebar(array(
		'id' => 'footer-col-3',
		'name' => __('Footer Column 3', 'jointstheme'),
		'description' => __('Footer Column 13', 'jointstheme'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	register_sidebar(array(
		'id' => 'footer-col-4',
		'name' => __('Footer Column 4', 'jointstheme'),
		'description' => __('Footer Column 4', 'jointstheme'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	register_sidebar(array(
		'id' => 'copyright',
		'name' => __('Copyright', 'jointstheme'),
		'description' => __('Copyright info', 'jointstheme'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));
	/*
	register_sidebar(array(
		'id' => 'az-col-1',
		'name' => __('AZ Database links  Column 1 - Category Search', 'jointstheme'),
		'description' => __('Category Search Widget', 'jointstheme'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	register_sidebar(array(
		'id' => 'az-col-2',
		'name' => __('AZ Database links  Column 2 - Vendor Search', 'jointstheme'),
		'description' => __('Vendor Search Widget', 'jointstheme'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));
	*/

	register_sidebar(array(
		'id' => 'az-sidebar',
		'name' => __('AZ Database links Sidebar', 'jointstheme'),
		'description' => __('AZ Database links Sidebar', 'jointstheme'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	/*
	to add more sidebars or widgetized areas, just copy
	and edit the above sidebar code. In order to call
	your new sidebar just use the following code:

	Just change the name to whatever your new
	sidebar's id is, for example:

	register_sidebar(array(
		'id' => 'sidebar2',
		'name' => __('Sidebar 2', 'jointstheme'),
		'description' => __('The second (secondary) sidebar.', 'jointstheme'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	To call the sidebar in your template, you can just copy
	the sidebar.php file and rename it to your sidebar's name.
	So using the above example, it would be:
	sidebar-sidebar2.php

	*/
} // don't remove this bracket!

/*********************
AZ DATAbase directory
*********************/

// List database links alphabetically code found here- http://wpsites.net/wordpress-tips/show-posts-by-post-title-in-alphabetical-order-for-custom-post-type-archive/

function wpsites_cpt_loop_filter($query) {
if ( !is_admin() && $query->is_main_query() ) {
	if ( is_post_type_archive('db_link')) {
	     $query->set( 'orderby', 'title' );
	     $query->set( 'order', 'ASC' );
	     $query->set( 'posts_per_page' , -1);
	    }
	  }
}

add_action('pre_get_posts','wpsites_cpt_loop_filter');

/*********************
COMMENT LAYOUT
*********************/

// Comment Layout
function joints_comments($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class('panel'); ?>>
		<article id="comment-<?php comment_ID(); ?>" class="clearfix large-12 columns">
			<header class="comment-author">
				<?php
				/*
					this is the new responsive optimized comment image. It used the new HTML5 data-attribute to display comment gravatars on larger screens only. What this means is that on larger posts, mobile sites don't have a ton of requests for comment images. This makes load time incredibly fast! If you'd like to change it back, just replace it with the regular wordpress gravatar call:
					echo get_avatar($comment,$size='32',$default='<path_to_url>' );
				*/
				?>
				<!-- custom gravatar call -->
				<?php
					// create variable
					$bgauthemail = get_comment_author_email();
				?>
				<?php printf(__('<cite class="fn">%s</cite>', 'jointstheme'), get_comment_author_link()) ?> on
				<time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time(__(' F jS, Y - g:ia', 'jointstheme')); ?> </a></time>
				<?php edit_comment_link(__('(Edit)', 'jointstheme'),'  ','') ?>
			</header>
			<?php if ($comment->comment_approved == '0') : ?>
				<div class="alert alert-info">
					<p><?php _e('Your comment is awaiting moderation.', 'jointstheme') ?></p>
				</div>
			<?php endif; ?>
			<section class="comment_content clearfix">
				<?php comment_text() ?>
			</section>
			<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
		</article>
	<!-- </li> is added by WordPress automatically -->
<?php
} // don't remove this bracket!

/*********************
Page subject field
*********************/

add_filter( 'cmb_meta_boxes', 'page_metaboxes' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function page_metaboxes( array $meta_boxes ) {

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_cmb_';

	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$meta_boxes['test_metabox'] = array(
		'id'         => 'test_metabox',
		'title'      => __( 'Page Title', 'cmb' ),
		'pages'      => array( 'page', ), // Post type
		'context'    => 'side',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		// 'cmb_styles' => true, // Enqueue the CMB stylesheet on the frontend
		'fields'     => array(
			array(
				'name'       => __( 'Alt page title', 'cmb' ),
				'desc'       => __( 'A different title that shows up on the page. Optional', 'cmb' ),
				'id'         => $prefix . 'subject',
				'type'       => 'text',
				'show_on_cb' => 'cmb_test_text_show_on_cb'
			)
		)
	);

	return $meta_boxes;
}

//add_action( 'init', 'page_initialize_cmb_meta_boxes', 9999 );

/**
 * Initialize the metabox class.
 */

/*********************
Snippet Shortcode
*********************/

function snippet_func( $atts ) {
	global $post;
	//echo $post->ID;


	$the_slug = 'snippet'; // <-- edit it
	//


	$args=array(
	  'name' => $atts['slug'],
	  'post_type' => 'snippet',
	  'post_status' => 'publish',
	  'numberposts' => 1
	);
	$myrow = get_posts($args);
	if($myrow){
		//foreach ( $myrows as $myrow )
		//{
		    $snippetID = $myrow[0]->ID;
		    $snip = get_post($snippetID);

			$meta_values = get_post_meta( $snippetID, '_cmb_embed_code' );


			$snippet_post = get_post($snippetID);
			$snippet = $snippet_post->post_content;
			//$snippet = apply_filters('the_content', $snippet);
			$snippet = wpautop( $snippet, false );


			//echo $post->ID.'start'.$snippet;
			return html_entity_decode($snippet);
		//}

	}
	else{
		echo 'Snippet not found';
	}


}
add_shortcode( 'snippet', 'snippet_func' );




/*********************
Excerpt
*********************/

function get_the_popular_excerpt(){
	$excerpt = get_the_content();
	$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, 50);
	$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
	$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
	//$excerpt = $excerpt.'... <a href="'.$permalink.'">more</a>';
	return $excerpt;
}



?>
