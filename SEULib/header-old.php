<!doctype html>

<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<title><?php wp_title(''); ?></title>

		<!-- Google Chrome Frame for IE -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<!-- mobile meta -->
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

		<!-- icons & favicons -->
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">

  	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">


		<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js'></script>

		<?php wp_head(); ?>

		<!-- Drop Google Analytics here -->
		<!-- end analytics -->


	</head>

	<body <?php body_class(); ?>>

	<div class="black-out"></div>

	<div class="off-canvas-wrap" data-offcanvas>
		<div class="inner-wrap">
			<div id="container">

				<header class="title-bar">
					<div id="inner-header" class="row">
						<div class="large-12 columns">
							<?php if( is_front_page() || is_archive() || is_home() || is_page_template('guides.php') ){ ?>
								<p class="dept"><a href="http://library.stedwards.edu">Munday Library</a></p>
								<h1><?php bloginfo('name'); ?></h1>
							<?php } else { ?>
								<?php if( is_page() ){ // If Research Guide

									if ( 0 == $post->post_parent ) { //  if this is the parent/home page display title
										?>
											<p class="dept">Munday Library</p>
											<h1><?php the_title(); ?></h1>
										<?php


									} else { //  if this is a parent page, find parent page title of subject title

									    $parent = array_reverse(get_post_ancestors($post->ID));
										$first_parent = get_page($parent[0]);

										$temp = $post;
										$post = get_post( $first_parent->ID );

										setup_postdata( $post );

										?>
										<p class="dept">Munday Library </p>
											<h1><?php echo $first_parent->post_title; ?></h1>
										<?php


										$post = $temp;

									}

								}
								else if( is_single()  ){?>
									<p class="dept">Munday Library</p>
									<h1><?php the_title(); ?></h1>

								<?php
								}


								else if(is_post_type_archive('db_link') or is_category() or is_tax('db_vendor')){
								?>
									<h1>A-Z Databases</h1>
									<span class="sub-header">Find the best library databases for your research</span>
								<?php
								}
								else{
								?>
									<p class="dept">Munday Library</p>
									<h1><?php bloginfo('name'); ?></h1>
								<?php
								}
								?>
							<?php } ?>
						</div>
					</div>

				</header>

				<?php // if( is_front_page() || is_archive() || is_home() || is_page_template('guides.php') ){ ?>
				<?php if( is_home()  ){ ?>


					<?php /*

					<header class="header" role="banner">

						<div id="inner-header" class="row">



								<div class="large-10 large-centered columns">
									<?php echo '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
		<input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="'.esc_attr__('Search Library Research Guides','jointstheme').'" />
		</form>'; ?></div>
								</div>

							*/	?>

					</header>
				<?php }  ?>




						 <?php //get_template_part( 'partials/nav', 'offcanvas' ); ?>

						 <?php get_template_part( 'partials/nav', 'topbar' ); ?>

						 <?php // get_template_part( 'partials/nav', 'offcanvas-sidebar' ); ?>

						<!-- You only need to use one of the above navigations.
							 Offcanvas-sidebar adds a sidebar to a "right" offcanavas menus. -->

					</div> <!-- end #inner-header -->

				</header> <!-- end header -->
