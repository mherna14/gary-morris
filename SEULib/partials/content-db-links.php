<?php


//var_dump($database_links);
if(function_exists('get_field')){
	$db_links = get_field('database_links');
	if( $db_links ): ?>
		<section id="db_links">
			<h3>Database Links</h3>
		    <ul>
		    <?php foreach( $db_links as $db_link): // variable must be called $post (IMPORTANT) ?>
		        <?php setup_postdata($db_link); ?>
		        <li>


		        		<b><a href="<?php  echo the_field('_cmb_url', $db_link->ID); ?>" target="_blank"><?php  echo get_the_title( $db_link->ID ); ?></a></b><br>
								<?php  echo get_the_content( $db_link->ID ); ?>


		        		</p>



		        </li>
		    <?php endforeach; ?>
		    </ul>
	</section>
	    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
	<?php endif;
}?>
