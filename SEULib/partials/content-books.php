<?php


//var_dump($books);
if(function_exists('get_field')){
	$books = get_field('book_links');
	if( $books ): ?>
		<section id="books">
			<h3>Books & E-books</h3>
		    <ul>
		    <?php foreach( $books as $book): // variable must be called $post (IMPORTANT) ?>
		        <?php setup_postdata($book); ?>
		        <li class="row">
		        	<div class="small-2 columns">

		        		<?php if ( has_post_thumbnail($book->ID) ) {

							$thumb_id = get_post_thumbnail_id($book->ID);
							$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
							$thumb_url = $thumb_url_array[0];

							?>

							<img src="<?php  echo $thumb_url; ?>">
						<?php } ?>
		        	</div>
		        	<div class="small-10 columns">
		        		<p>
		        		<b><a href="<?php  echo the_field('book_link', $book->ID); ?>" target="_blank"><?php  echo get_the_title( $book->ID ); ?></a> by <?php  echo the_field('book_author', $book->ID); ?></a></b><br>
		        		ISBN: <?php echo the_field('book_isbn', $book->ID); ?><br><i>
		        		<b>Publication Date: </b><?php echo the_field('publication_date', $book->ID); ?></i>
		        		</p>
		        		<?php //var_dump($book); ?>
		        	</div>


		        </li>
		    <?php endforeach; ?>
		    </ul>
	</section>
	    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
	<?php endif;
}?>
