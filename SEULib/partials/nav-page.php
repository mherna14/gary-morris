<?php
	$pagelist = get_pages('sort_column=menu_order&sort_order=asc');
	$pages = array();
	foreach ($pagelist as $page) {
	   $pages[] += $page->ID;
	}

	$current = array_search($post->ID, $pages);
	if($current > 0){
		$prevID = $pages[$current-1];
	}
	if($current < count($pagelist)-1 ){
		$nextID = $pages[$current+1];
	}
	?>

	<div class="page-nav row">
		<div class="large-6 medium-6 small-6 columns">
			<?php if (!empty($prevID) and 0 != $post->post_parent and $current > 0) { ?>
				
				<a href="<?php echo get_permalink($prevID); ?>"
				  title="<?php echo get_the_title($prevID); ?>"
				  class="button secondary tiny radius left">Previous</a>
				
			<?php } ?>
		</div>
		<div class="large-6 medium-6 small-6 columns">
			<?php if (!empty($nextID)) { ?>
				
				<a href="<?php echo get_permalink($nextID); ?>" 
				 title="<?php echo get_the_title($nextID); ?>" 
				 class="button secondary tiny radius right">Next</a>
			<?php } ?>
		</div>
	</div>