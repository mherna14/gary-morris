

    <section class="entry-content clearfix" itemprop="articleBody">
    	<header class="article-header">



			<?php

			if ( 0 == $post->post_parent ) { //  if this is the parent/home page display title
				?>

				<?php

				if ( has_excerpt( get_the_ID() ) ) {?>
			    	<span class="sub-header"><?php echo get_the_excerpt();?></span>
			    <?php
				}

			}

			$get_subject_title = get_post_meta( get_the_ID(), '_cmb_subject' );

			if($get_subject_title){
				$subject_title = $get_subject_title[0];
			}else{
				$subject_title = get_the_title();
			}
			?>
			<h2 class="page-title"><?php echo $subject_title ?></h2>
		</header> <!-- end article header -->



	    <?php the_content(); ?>

	</section> <!-- end article section -->
