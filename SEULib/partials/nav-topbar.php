

<div class="large-12 columns">
	<div class="fixed">

	<!-- If you want to use the more traditional "fixed" navigation.
		 simply replace "sticky" with "fixed" -->

			<nav class="top-bar" data-topbar>

				<div class="row">

					<ul class="title-area">
						<!-- Title Area -->
						<li class="name">
							<div id="seu-logo"></div>
						</li>
						<li class="toggle-topbar menu-icon">
							<a href="#"><span>Menu</span></a>
						</li>
					</ul>
					<div id="top-nav" class="right">

						<?php if( !is_front_page()){
						 		echo '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" ><input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="'.esc_attr__('Search Library Research Guides','jointstheme').'" />
</form>';
//<a href="http://ask.stedwards.edu/" class="button search"><i class="fa fa-search"></i></a>
						}?>
						<?php //joints_main_nav(); ?>


						<a class="button toggle">
		              <div class="hamburger top-bar"></div>
		              <div class="hamburger mid-bar"></div>
		              <div class="hamburger mid-bar2"></div>
		              <div class="hamburger bottom-bar"></div>



						</a>


						<div id="nav-dropdown">
							<?php

							// get parent pages exclude profile and guides page


							$pages = get_pages('parent=0&sort_order=ASC'); // Get published parent pages only

							if($pages  > 0){ 	?>

								<ul id="top-menu" class="side-nav">
								<?php

									foreach($pages as $page){

										if($page->post_excerpt){
											$excerpt = $page->post_excerpt;
										}
										else{
											$excerpt = $page->post_title;
										}
											?>

										<li><a href="<?php echo home_url('/').$page->post_name; ?>" ><?php echo $page->post_title ?></a></li>


										<?php } 	?>
										<?php if ( is_active_sidebar( 'offcanvas' ) ) {
											 dynamic_sidebar( 'offcanvas' ); }?>

									</ul>

									<?php
								}

							?>

						</div>

					</div>
				</div>


			</nav>

	</div>
</div>
