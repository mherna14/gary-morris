<?php
	$link_url = get_post_meta( get_the_ID(), '_cmb_url' );
	$link_url = $link_url [0];
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">

	<header class="article-header">
		<h4><a href="<?php echo $link_url; ?>" rel="bookmark" title="<?php the_title_attribute(); ?>" target="_blank"> <?php the_title(); ?></a></h4>
		
	</header> <!-- end article header -->
					
	<section class="entry-content clearfix" itemprop="articleBody">
		<?php the_content(); ?>
	</section> <!-- end article section -->
						
	<footer class="article-footer">
		<p class="byline"><?php the_category(', ') ?></p>	
    	<p class="tags"><?php the_tags('<span class="tags-title">' . __('Tags:', 'jointstheme') . '</span> ', ', ', ''); ?></p>
		
	</footer> <!-- end article footer -->
						    
	
					
</article> <!-- end article -->
