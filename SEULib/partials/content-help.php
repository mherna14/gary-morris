<?php
//var_dump($help_desk_links);
if(function_exists('get_field')){
	$help_links = get_field('help_desk_links');
	if( $help_links ): ?>
		<aside id="help_links" class="large-3 medium-3 medium-push-3 columns panel" role="complementary" >
			<h3>Need Help?</h3>
				<ul>
				<?php foreach( $help_links as $help_link): // variable must be called $post (IMPORTANT) ?>
						<?php setup_postdata($help_link); ?>
						<li>
								<a href="<?php  echo the_field('_cmb_help_url', $help_link->ID); ?>" target="_blank"><?php  echo get_the_title( $help_link->ID ); ?></a>

						</li>
				<?php endforeach; ?>
				</ul>
	</aside>
			<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
	<?php endif;
}?>
