<?php

//var_dump($database_links);
if(function_exists('get_field')){
	$questions = get_field('question');
	if( $questions ): ?>
		<section id="questions">

		    <?php foreach( $questions as $question): // variable must be called $post (IMPORTANT) ?>
		        <?php setup_postdata($question); ?>

		        <div class="row ques">
							<div class="small-2 columns">
								<h3 class="q_a">Q</h3>
							</div>
							<div class="small-10 columns">
								<h3><?php  echo get_the_title( $question->ID ); ?></h3>
							</div>
						</div>

						<div class="row">
							<div class="small-2 columns">
								<h3 class="q_a">A</h3>
							</div>
							<div class="small-10 columns">
								<?php  echo get_the_content( $question->ID ); ?>
							</div>
		        </div>

		    <?php endforeach; ?>

	</section>
	    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
	<?php endif;
}?>
