<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
	<?php if(has_post_thumbnail()){  ?>

	<div class="small-6 columns">
		<?php the_post_thumbnail('medium'); 
		$col = '6';

		?>	
	</div>

	<?php  } else { 

		$col = '12';

	 } ?>
	
	<div class="small-<?php echo $col; ?> columns">
		<header class="article-header">
			<h4><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
			<?php get_template_part( 'partials/content', 'byline' ); ?>
		</header> <!-- end article header -->
						
		<section class="entry-content clearfix" itemprop="articleBody">
			
			<p><?php echo get_the_popular_excerpt(); ?></p>
			<p class="topics">Topics: <?php echo get_the_category_list(', ');  ?></p>
		</section> <!-- end article section -->
							
		<footer class="article-footer">
	    	<p class="tags"><?php the_tags('<span class="tags-title">' . __('Tags:', 'jointstheme') . '</span> ', ', ', ''); ?></p>
		</footer> <!-- end article footer -->
						    
	</div>
					
</article> <!-- end article -->
