<?php get_header(); ?>

			<div id="content" role="main">
				<div  class="row clearfix">
					<div class="large-12 columns">
						<?php the_breadcrumb(); ?>

					</div>
				</div>
				<div id="inner-content" class="row clearfix">



				    <div class="large-8 columns clearfix" >


							<?php
							$pages = get_pages('post_type=site&sort_order=DESC&parent=0'); // Get published parent pages only
							if($pages  > 0){
								?>



								<?php


								foreach($pages as $page){

									if($page->post_excerpt){
										$excerpt = $page->post_excerpt;
									}
									else{
										$excerpt = $page->post_title;
									}


										//$children = get_pages('child_of='.$page->ID);
										$level = count(get_post_ancestors( $page->ID ));

										//echo count(get_post_ancestors( $page->ID )) ;
										?>

									<?php

									if($level == 0){	?>

									<h2><?php echo $page->post_title ?></h2>
								<?php
									echo '<ul class="menu-homepage">';
									//$parent = get_top_parent_page_id($page->ID);
									// use wp_list_pages to display parent and all child pages all generations (a tree with parent)
									$args=array(
										'post_parent' => $page->ID,
										'post_type'=>'site',
										'sort_order'=>'ASC'
									);
									$sites = get_children($args);
									//echo $sites;
									if ($sites) {
										$siteids = array();

										foreach ($sites as $site) {
											$siteids[]= $site->ID; ?>
											<li>
												<span class="accordion-plus"></span>
												<h3><a href="<?php echo  $site->post_name; ?>" ><?php echo $site->post_title ?></a></h3>

												<?php
													$years_arg=array(
														'post_parent' => $site->ID
													);
													$years = get_children($years_arg);
													if($years){?>
														<span class="info">
															<?php the_excerpt(); ?>
														<h4>Years</h4>
														<?php
														foreach ($years as $year) { ?>
															<h4><a href="<?php echo  $year->post_name; ?>" ><?php echo $year->post_title ?></a></h4>

														<?php	} ?>

														</span>
													<?php
													}else { ?>
														<span class="info"> <?php the_excerpt(); ?></span>
													<?php	} ?>
											</li>
										<?php }


									}
									echo '</ul>';


									}

									}

							}

						?>



							<?php
							//wp_list_categories('hide_empty=0&show_count=1&title_li=<h2>' . __('') . '</h2>&show_count=0' ); ?>




				    </div> <!-- end #main -->

				    <?php get_sidebar(); ?>

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>
