
<?php get_header(); ?>

			<div id="content">

				<div  class="row clearfix">
					<div class="large-12 columns">
						<?php the_breadcrumb(); ?>
					</div>
				</div>

				<div id="inner-content" class="row clearfix">

				    <main id="main" class="large-12 columns" role="main">

					    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
								<span id="dataFile"><?php $dataFile = get_post_meta( get_the_ID(), '_cmb_data-file' ); echo $dataFile[0];?></span>

					    	<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">



							    <section class="entry-content clearfix" itemprop="articleBody">
										<header>
											<h2 id="site-name"></h2>
        							<h3 id="launch-date"></h3>
						          <ul id="info"></ul>
						        </header>
						        <svg id="visualisation"></svg>


									</section> <!-- end article section -->



								<footer class="article-footer">
									<p class="tags"><?php the_tags('<span class="tags-title">' . __('Tags:', 'jointstheme') . '</span> ', ', ', ''); ?></p>	</footer> <!-- end article footer -->

								<?php comments_template(); ?>

							</article> <!-- end article -->


					    <?php endwhile; else : ?>

					   		<?php get_template_part( 'partials/content', 'missing' ); ?>

					    <?php endif; ?>

    				</main > <!-- end #main -->



				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>
