<?php
/*
Template Name: Display Authors
*/

// Get all users order by amount of posts
$allUsers = get_users('orderby=last_name&order=DESC');

$users = array();

// Remove subscribers from the list as they won't write any articles
foreach($allUsers as $currentUser)
{
	if(!in_array( 'subscriber', $currentUser->roles ))
	{
		$users[] = $currentUser;
	}
}

?>

<?php get_header(); ?>

			<div id="content">
			
				<div id="inner-content" class="row clearfix">

				    <main id="main" class="large-8 medium-8 columns" role="main">

					<ul id="profiles" class="small-block-grid-4">

					<?php
						

						foreach($users as $user)
						{
							?>
							
							<?php
							
								$first_name = $user->first_name;
								$last_name = $user->last_name; 
								$nickname = $user->nickname;
								$userID = $user->ID;
								$userImage = get_cupp_meta($userID, 'thumbnail' )
								?>
								<li><a href="<?php echo home_url().'/author/'.$nickname; ?>"><img src="<?php echo $userImage; ?>"></a><b> <?php echo $first_name.' '.$last_name; ?></b></li>

							<?php
							}
							?>
						</ul>

					</main > <!-- end #main -->

				  	<?php get_sidebar(); ?>
    

				    
				</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>