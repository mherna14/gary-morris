<?php
/*
Template Name: Guide Home page - nested page
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div  class="row clearfix">
					<div class="large-12 columns">
						<?php the_breadcrumb(); ?>
					</div>
				</div>

				<div id="inner-content" class="row clearfix">

				    <main id="main" class="large-9 medium-push-3 medium-9 columns" role="main">


					    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					    	<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/WebPage">

								<?php get_template_part( 'partials/loop', 'page' ); ?>


						    	<?php // Display next level only children in blocks
						    		$args = array(
									        'child_of' => $post->ID,
									        'parent' => $post->ID,
									        'hierarchical' => 0,
									        'sort_column' => 'menu_order',
									        'sort_order' => 'asc'
									);
									$num_children = count(get_pages($args));
									$num_of_columns = 3;

									/*if( $num_children % 4 == 0 or $num_children > 9){
										$num_of_columns = 4;
									}*/

									if($num_children > 0){
										echo '<section>';
										echo '<h3>Contents</h3>';

										$children = wp_list_pages('title_li=&child_of='.$post->ID.'&echo=0&depth=0');

									  	if ($children) {
										  	echo '<ul class="large-block-grid-'.$num_of_columns.'  medium-block-grid-2  small-block-grid-1 guide-contents nested">';
											 echo  $children;
											echo '</ul>';

										}

						    			echo '</section>';
								 } ?>

								 <?php get_template_part( 'partials/content', 'books' ); ?>

							</article> <!-- end article -->


					    	<footer class="article-footer">
				    			<?php get_template_part( 'partials/nav', 'page' ); ?>
								<?php the_tags( 'Tags: ', ', ', '<br />' ); ?>
							</footer> <!-- end article footer -->



					    <?php endwhile; else : ?>

					   		<?php get_template_part( 'partials/content', 'missing' ); ?>

					    <?php endif; ?>

    				</main > <!-- end #main -->

				    <?php get_sidebar('page'); ?>

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>
