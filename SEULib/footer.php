

					<footer id="footer" role="contentinfo">

						<div id="inner-footer" class="row clearfix">

							<div class="large-12 medium-12 columns">
								<div class="large-3 medium-3 columns">
									<img src="<?php echo get_template_directory_uri(); ?>/library/img/logo-white.png">
									<?php if ( is_active_sidebar( 'footer-col-1' ) ) : ?>
										<?php dynamic_sidebar( 'footer-col-1' ); ?>
									<?php else : ?>
										<p>Please add links</p>
									<?php endif; ?>
								</div>

								<div class="large-3 medium-3 columns">
									<?php if ( is_active_sidebar( 'footer-col-2' ) ) : ?>
										<?php dynamic_sidebar( 'footer-col-2' ); ?>
									<?php else : ?>
										<p>Please add links</p>
									<?php endif; ?>

								</div>

								<div class="large-3 medium-3 columns">
									<?php if ( is_active_sidebar( 'footer-col-3' ) ) : ?>
										<?php dynamic_sidebar( 'footer-col-3' ); ?>
									<?php else : ?>
										<p>Please add links</p>
									<?php endif; ?>

								</div>

								<div class="large-3 medium-3 columns">
									<?php if ( is_active_sidebar( 'footer-col-4' ) ) : ?>
										<?php dynamic_sidebar( 'footer-col-4' ); ?>
									<?php else : ?>
										<p>Please add links</p>
									<?php endif; ?>

								</div>




		    				</div>


						</div> <!-- end #inner-footer -->
					</footer> <!-- end .footer -->
				</div> <!-- end #container -->
			</div> <!-- end .inner-wrap -->
		</div> <!-- end .off-canvas-wrap -->

				<!-- all js scripts are loaded in library/joints.php -->
				<?php wp_footer(); ?>
	</body>

</html> <!-- end page -->
