<?php get_header(); ?>

			<div id="content">
				<div  class="row clearfix">
					<div class="large-12 columns">
						<?php the_breadcrumb(); ?>
					</div>
				</div>

				<div id="inner-content" class="row clearfix">



					<main id="main" class="large-12 columns" role="main">

						<?php
							$term = get_queried_object();

							$children = get_terms( $term->taxonomy, array(
							'parent'    => $term->term_id,
							'hide_empty' => false
							) );
							// print_r($children); // uncomment to examine for debugging
							if($children) { // get_terms will return false if tax does not exist or term wasn't found.
									// term has children
									echo "has children";
									$this_cat = get_query_var('cat'); // get the category of this category archive page
									wp_list_categories('child_of=' . $this_cat . '&title_li='); // list child categories
							}else{
								echo '<span id="api">';
								$cat = get_query_var('cat');
								$yourcat = get_category ($cat);
								echo $yourcat->slug;
								echo '</span>';
							}
						?>



						<section class="entry-content clearfix" itemprop="articleBody">
							<div id="parse">



							</div>
						</section>








				    </main> <!-- end #main -->


				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>
