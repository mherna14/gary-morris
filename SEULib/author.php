
<?php get_header(); ?>

<?php
	if(isset($_GET['author_name'])) :
		$curauth = get_userdatabylogin($author_name);
	else :
		$curauth = get_userdata(intval($author));
	endif;

	$userID = $curauth->ID;
	$userImage = get_cupp_meta($userID, 'Full Size' );
	$user_email = $curauth->user_email;
?>
			
			<div id="content">
			
				<div id="inner-content" class="row clearfix">

					<nav id="sidebar" class="sidebar large-3 medium-3 columns" role="complementary">

						<div id="profile">
							<img src="<?php echo $userImage; ?>">
							<a href="mailto:<?php echo $user_email; ?>" class="button radius medium email-me">Email Me</a>
						</div>
						

					</nav>
			
				    <main id="main" class="large-9 medium-9 columns" role="main">



						
						<?php

							$getProfilesPage = get_page_by_path('profiles');
						    if ($getProfilesPage) {
						        $getProfilesPageID =  $getProfilesPage->ID;
						    } else {
						        $getProfilesPageID =  '';
						    }
							$args = array(
								'sort_order' => 'ASC',
								'sort_column' => 'post_title',
								'hierarchical' => 1,
								'exclude' => $getProfilesPageID,
								'include' => '',
								'meta_key' => '',
								'meta_value' => '',
								'authors' => $curauth->nickname,
								'child_of' => 0,
								'parent' => 0,
								'exclude_tree' => '',
								'number' => '',
								'offset' => 0,
								'post_type' => 'page',
								'post_status' => 'publish'
							); 

							$pages = get_pages($args); // Get published parent pages only 
							if($pages  > 0){
								?>

								<h3>Showing <?php echo count( $pages );?> Guides</h3>
								<ul id="menu-homepage">
								<?php
								
								
								foreach($pages as $page){
									$user_info = get_userdata($page->post_author); // Get Author's info
								    $username = $user_info->user_login;
								    $first_name = $user_info->first_name;
								    $last_name = $user_info->last_name;

									if($page->post_excerpt){
										$excerpt = $page->post_excerpt;
									}
									else{
										$excerpt = $page->post_title;
									}
								    ?>

									<li>
										<a href="<?php echo home_url().'/'. $page->post_name; ?>" ><?php echo $page->post_title ?></a>
										<span data-tooltip aria-haspopup="true" data-options="hover_delay: 500;" class="has-tip tip-top radius" title="<?php echo $excerpt.'<br><i>by '.$first_name.' '.$last_name.'</i>'; ?>"><i class="fa fa-info-circle"></i></span>
										<em class="date"><?php echo  mysql2date('M j Y',$page->post_date); ?></em>
									</li>

									<?php
								} 

								 ?>
								</ul>
							<?php
							}

						?>

				
			
    				</main > <!-- end #main -->
    

				    
				</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>