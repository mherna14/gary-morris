<?php get_header(); ?>
<?php
$help_links = null;
$main_column = '9';
//var_dump($help_desk_links);

		?>



			<div id="content">

				<div  class="row clearfix">
					<div class="large-12 columns">
						<?php the_breadcrumb(); ?>
					</div>

				</div>


				<div id="inner-content" class="row clearfix">



				    <main id="main" class="large-<?php echo $main_column; ?> medium-push-3 medium-<?php echo $main_column; ?> columns" role="main">

					    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					    	<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/WebPage">

								<?php get_template_part( 'partials/loop', 'page' ); ?>





							</article> <!-- end article -->



					    <?php endwhile; else : ?>

					   		<?php get_template_part( 'partials/content', 'missing' ); ?>

					    <?php endif; ?>

    				</main > <!-- end #main -->

						<?php

						if( $help_links ){
							get_template_part( 'partials/content', 'help' );
						}

						?>

				    <?php get_sidebar('page'); ?>

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>
