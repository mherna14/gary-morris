<?php
/* joints Custom Post Type Example
This page walks you through creating
a custom post type and taxonomies. You
can edit this one or copy the following code
to create another one.

I put this in a separate file so as to
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

*/


// let's create the function for the custom type
function custom_post_launch() {
	// creating (registering) the custom type
	register_post_type( 'launch', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Launch', 'jointstheme'), /* This is the Title of the Group */
			'singular_name' => __('Launch', 'jointstheme'), /* This is the individual type */
			'all_items' => __('All Launches', 'jointstheme'), /* the all items menu item */
			'add_new' => __('Add New Launch', 'jointstheme'), /* The add new menu item */
			'add_new_item' => __('Add Launch', 'jointstheme'), /* Add New Display Title */
			'edit' => __( 'Edit', 'jointstheme' ), /* Edit Dialog */
			'edit_item' => __('Edit Launches', 'jointstheme'), /* Edit Display Title */
			'new_item' => __('New Launches', 'jointstheme'), /* New Display Title */
			'view_item' => __('View Launches', 'jointstheme'), /* View Display Title */
			'search_items' => __('Search Launches', 'jointstheme'), /* Search Custom Type Title */
			'not_found' =>  __('Nothing found in the Database.', 'jointstheme'), /* This displays if there are no entries yet */
			'not_found_in_trash' => __('Nothing found in Trash', 'jointstheme'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the example custom post type', 'jointstheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' => get_stylesheet_directory_uri() . '/library/img/custom-post-icon.png', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'launch', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'launch', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author')
	 	) /* end of options */
	); /* end of register post type */

	/* this adds your post categories to your custom post type */
	register_taxonomy_for_object_type('category', 'launch');
	/* this adds your post tags to your custom post type */
	//register_taxonomy_for_object_type('post_tag', 'launch');

}



	// adding the function to the Wordpress init
	add_action( 'init', 'custom_post_launch');






?>
