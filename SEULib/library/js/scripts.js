/*
Joints Scripts File

This file should contain any js scripts you want to add to the site.
Instead of calling it in the header or throwing it inside wp_head()
this file will be called automatically in the footer so as not to
slow the page load.

*/

// IE8 ployfill for GetComputed Style (for Responsive Script below)
/*jshint -W098 */
if (!window.getComputedStyle) {
    window.getComputedStyle = function(el, pseudo) {
        'use strict';
        this.el = el;
        this.getPropertyValue = function(prop) {
            var re = /(\-([a-z]){1})/g;
            if (prop === 'float') {
                prop = 'styleFloat';
            }
            if (re.test(prop)) {
                prop = prop.replace(re, function () {
                    return arguments[2].toUpperCase();
                });
            }
            return el.currentStyle[prop] ? el.currentStyle[prop] : null;
        };
        return this;
    };
}

// as the page loads, call these scripts
jQuery(document).ready(function($) {

    /*
    Responsive jQuery is a tricky thing.
    There's a bunch of different ways to handle
    it, so be sure to research and find the one
    that works for you best.
    */

    /* getting viewport width */
    'use strict';
    var responsive_viewport = $(window).width();

    /* if is below 481px */
    if (responsive_viewport < 481) {

    } /* end smallest screen */

    /* if is larger than 481px */
    if (responsive_viewport > 481) {

    } /* end larger than 481px */

    /* if is above or equal to 768px */
    if (responsive_viewport >= 768) {

        /* load gravatars */
        $('.comment img[data-gravatar]').each(function(){
            $(this).attr('src',$(this).attr('data-gravatar'));
        });

    }

    /* off the bat large screen actions */
    if (responsive_viewport > 1030) {

    }

    //Move alphabet menu up.


    //top menu nav
    $('.toggle, .black-out').click(function (e) {
      $('#nav-dropdown').toggleClass('open');
      $('.black-out').toggleClass('open');
      $('.toggle').toggleClass('open');

    });

    // Add carets to menu items with sub pages.
    $( '#sidebar .page_item_has_children' ).each(function( index ) {
        if(index === 0){ // account for the top menu item


        }
        else{
            if( $(this).hasClass('current_page_item')){
                $(this).children( 'a' ).append('<i class="fa fa-caret-down"></i>');
            }else{
                $(this).children( 'a' ).append('<i class="fa fa-caret-left"></i>');
            }
        }

    });

    $('.accordion-plus').click(function (e) {
	          $(this).parent().toggleClass('open');
	          if ($(this).parent().hasClass('open')) {
	            $(this).siblings('.info').slideDown();
	          } else {
	            $(this).siblings('.info').slideUp();
	          }
	        });




   $(".navbar .container").prepend('<a href="#" class="open-panel-2"><i class="fa fa-bars"></i></a>');

          $(".open-panel-2").click(function(){

            if ( $("html").hasClass("openNav") ) {
                $("html").removeClass("openNav");
            }
            else{
                $("html").addClass("openNav");
            }

            });

            $(".openNav .open-panel-2, #content").click(function(){

                 $("html").removeClass("openNav");

            });

            $('.dropdown a').click(function() {


                if($(this).parent().hasClass('active')){
                    $(this).parent().removeClass('active');
                }
                else{
                    $('.dropdown.active').removeClass('active');
                    $(this).parent().addClass('active');
                }


                return false;
          });

             $( window ).resize(function() {
                setWindowSize();
            });
            function setWindowSize(){
                var w = window.innerWidth;

                if(w > 880 && $("html").hasClass("openNav")){
                    $("html").removeClass("openNav");

                }
            }

            //$( "#resources .block a" ).tooltip({ content: $(this).children('img').attr("alt") });

            var isFiltered = false;
            var getFilter = '';

            $('#filter_btns a').click(function() {


                if(!isFiltered && getFilter === ''){
                    getFilter = $(this).attr('id');
                    console.log(getFilter);

                    $(this).addClass('filtered');
                    $( '#resources .block a' ).each(function( index ) {
                        if( !$(this).hasClass(getFilter)){
                            $(this).addClass('dim');
                        }
                    });
                    isFiltered = true;
                }
                else if( isFiltered && getFilter != $(this).attr('id') ){
                    console.log("2 " + getFilter);
                    $('.filtered').removeClass('filtered');
                    getFilter = $(this).attr('id');

                    $(this).addClass('filtered');
                     $( '#resources .block a' ).each(function( index ) {
                        if( !$(this).hasClass(getFilter)){
                            $(this).addClass('dim');
                        }
                        else{
                            $(this).removeClass('dim');
                        }
                    });

                }
                else{
                    $(this).removeClass('filtered');
                    $( '#resources .block a' ).each(function( index ) {
                        if( !$(this).hasClass(getFilter)){
                            $(this).removeClass('dim');
                        }

                    });
                    getFilter = '';
                    isFiltered = false;
                }

                return false;
          });

          var jsonFile
          var isTitle = true;
          var dataInterval = 20;
          var startData;
          var startNum;
          var n = 0;
          var launch = {

              info: [],
          		data: [],

          };


  function getDatFile(dataFile){
		jQuery.ajax({
			url:dataFile,
      crossDomain: true,
			success: function(result) {
					var content = result;
					var lines = content.split('\n');

					content.split(" ");

					var getInfo = String(lines[1]);
					var InfoSlice = getInfo.split('Created ');
					var InfoSlice2 = InfoSlice[1].split(' for flight on ');

					launch.created = InfoSlice2[0];
					launch.title = InfoSlice2[1];

					for(var i=0; i < lines.length; i++){

						var line = String(lines[i]);
						line = line.split(/[ ,]+/).join(',')
						var array = line.split(',');


						if( array.indexOf(":") > -1  && isTitle){

							var lineLabel = array.splice(0,array.indexOf(":"));
							var lineValue = array.splice(array.indexOf(":")+ 1);

							lineLabel.toString();
							lineValue.toString();

							var Label = String(lineLabel).replace(/,/g," ");
							var Value = String(lineValue).replace(/,/g," ");

							var newline = [];
							newline[0]= Label;
							newline[1]= Value;
							launch.info.push({
									"label" : Label,
									"value"  : Value
							});
						}

						else if(array[0] === ''){

							array.shift();
							if(isTitle){
								isTitle = false;
								startData = i+3;
							}

							if(i === startData){
								n = Number(array[0]);
							}

							if(array[0] === String(n) || Number(array[0]) > Number(n) ){
								launch.data.push({
										"time" : array[0],
										"press"  : array[1],
										"alt"  : array[2],
                    "temp"  : array[3],
                    "rh"  : array[4],
                    "mPa"  : array[5],
                    "ppmv"  : array[6],
                    "du"  : array[7],
                    "deg"  : array[8],
                    "spd"  : array[9],
                    "pump"  : array[10],
                    "uA"  : array[11]
								});
								n = n + dataInterval;

							}

						}

					}
          console.log(launch);


          // Title, x value, y value, x label, y label

					lineChart('Time over Pressure','time', 'press', 'Time (secs)', 'Pressure (hPa)');

          lineChart('Time over Alt','time', 'alt', 'Time (secs)', 'Alt (km)');  // create graph

          lineChart('Time over uA','time', 'uA', 'Time (secs)', 'uA');  // create graph

          dotChart('Partial Pressure over altitude','mPa', 'alt', 'mPa', 'KM');

          dotChart('Temperature over Pressure','temp', 'press', 'Temp', 'Press');  // create graph

          dotChart('Wind Direction over Altitude','deg', 'alt', 'deg', 'Alt (km)');  // create graph

          dotChart('Mixing Ratio over Altitude','ppmv', 'alt', 'ppmv', 'Alt (km)');  // create graph



          displayInfo(); // display header info


			},
		});
	}



function lineChart(vtitle, xvalue, yvalue, xlabel, ylabel) {
  var vid = vtitle.replace(/\s+/g, '-').toLowerCase();
  console.log(vid);

  $("#visuals").append('<div id="'+vid+'" class="svg-graph"></div>');

  $("#"+vid).append('<h2>'+vtitle+'</h2>');




  var margin = {top: 20, right: 50, bottom: 30, left: 50},
    width = 960 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

var bisectDate = d3.bisector(function(d) { return d[xvalue]; }).left;


var x = d3.scale.linear()
    .range([0, width]);

var y = d3.scale.linear()
    .range( [height, 0]);

var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom");

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left");



var line = d3.svg.line()
    .x(function(d) { return x(d[xvalue]); })
    .y(function(d) { return y(d[yvalue]); });

var svg = d3.select("#"+vid).append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom + 50)
    .attr("id", vid+'-svg')
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
    .style("text-rendering", "optimizeLegibility")
    .style("shape-rendering", "default");


    launch.data.forEach(function(d) {
      d[xvalue] = +d[xvalue];
      d[yvalue] = +d[yvalue];
    });



    launch.data.sort(function(a, b) {
      return a[xvalue] - b[xvalue];
    });
    var data = launch.data
    var firstline = launch.data[0];
    var lastline = launch.data[launch.data.length - 1];
    x.domain([firstline[xvalue], lastline[xvalue]]);
    y.domain(d3.extent(launch.data, function(d) { return d[yvalue]; }));


    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + (height) + ")")
        .call(xAxis)
      .append("text")
        .attr("class", "x label")
        .attr("text-anchor", "front")
        .attr("x", width)
        .attr("y", -6)
        .text(xlabel);

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
      .append("text")
        .attr("transform", "rotate(-90)")
        .attr("class", "y label")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text(ylabel);

    svg.append("path")
        .datum(data)
        .attr("class", "line")
        .attr("d", line);

    var focus = svg.append("g")
        .attr("class", "focus")
        .style("display", "none");

    focus.append("circle")
        .attr("r", 4.5);

    focus.append("text")
        .attr("x", 9)
        .attr("dy", ".35em");

    svg.append("rect")
        .attr("class", "overlay")
        .attr("width", width)
        .attr("height", height+50)
        .on("mouseover", function() { focus.style("display", null); })
        .on("mouseout", function() { focus.style("display", "none"); })
        .on("mousemove", mousemove);

  /*
        // style plot elements
      d3.selectAll("path.simpleline").
        style("opacity", "1").
        style("stroke-width", "1").
        style("stroke", "black").
        style("fill", "none");

    // style the axes
      d3.selectAll("path.domain").
        style("opacity", "0.4").
        style("stroke-width", "1").
        style("fill", "black");

      d3.selectAll(".tick").
        style("opacity", "0.8").
        style("stroke", "black").
        style("stroke-width", "1");

      */

    function mousemove() {
      var x0 = x.invert(d3.mouse(this)[0]),
          i = bisectDate(data, x0, 1),
          d0 = data[i - 1],
          d1 = data[i];
          //console.log(d1[xvalue]);
          var d = x0 - d0[xvalue] > d1[xvalue] - x0 ? d1 : d0;

      focus.attr("transform", "translate(" + x(d[xvalue]) + "," + y(d[yvalue]) + ")");
      focus.select("text").html(d[xvalue]+" , "+d[yvalue]);
    }
    var print_btn = 'print-'+vid;

    $("#"+vid).append('<a class="print-btn" id="'+print_btn+'">Print this graph only</a>');

    $("#"+print_btn).click(function() { print_svg(vid); });
/*
    var html = d3.select("svg")
        .attr("title", vtitle)
        .attr("version", 1.1)
        .attr("xmlns", "http://www.w3.org/2000/svg")
        .node().parentNode.innerHTML;
d3.select("#visuals").append("div")
        .attr("id", "download")
        .html("Right-click on this preview and choose Save as<br />Left-Click to dismiss<br />")
        .append("img")
        .attr("src", "data:image/svg+xml;base64,"+ btoa(html));

*/


}

function dotChart(vtitle, xvalue, yvalue, xlabel, ylabel) {
  var vid = vtitle.replace(/\s+/g, '-').toLowerCase();
  console.log(vid);

  $("#visuals").append('<div id="'+vid+'" class="svg-graph"></div>');

  $("#"+vid).append('<h2>'+vtitle+'</h2>');

  var margin = {top: 20, right: 20, bottom: 30, left: 40},
    width = 960 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

/*
 * value accessor - returns the value to encode for a given data object.
 * scale - maps value to a visual display encoding, such as a pixel position.
 * map function - maps from data value to display value
 * axis - sets up axis
 */

// setup x
var xValue = function(d) { return d[xvalue];}, // data -> value
    xScale = d3.scale.linear().range([0, width]), // value -> display
    xMap = function(d) { return xScale(xValue(d));}, // data -> display
    xAxis = d3.svg.axis().scale(xScale).orient("bottom");

// setup y
var yValue = function(d) { return d[yvalue];}, // data -> value
    yScale = d3.scale.linear().range([height, 0]), // value -> display
    yMap = function(d) { return yScale(yValue(d));}, // data -> display
    yAxis = d3.svg.axis().scale(yScale).orient("left");

// setup fill color
var cValue = function(d) { return d[xvalue];},
    color = d3.scale.category10();

// add the graph canvas to the body of the webpage
var svg = d3.select("#"+vid).append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .attr("id", vid)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

// add the tooltip area to the webpage
var tooltip = d3.select("body").append("div")
    .attr("class", "tooltip")
    .style("opacity", 0);




// load data
//d3.parse(launch.data, function(error, data) {

  // change string (from CSV) into number format
  launch.data.forEach(function(d) {
    d[xvalue] = +d[xvalue];
    d[yvalue] = +d[yvalue];
//    console.log(d);
  });

  // don't want dots overlapping axis, so add in buffer to data domain
  xScale.domain([d3.min(launch.data, xValue)-1, d3.max(launch.data, xValue)+1]);
  yScale.domain([d3.min(launch.data, yValue)-1, d3.max(launch.data, yValue)+1]);

  // x-axis
  svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis)
    .append("text")
      .attr("class", "label")
      .attr("x", width)
      .attr("y", -6)
      .style("text-anchor", "end")
      .text(xlabel);

  // y-axis
  svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
    .append("text")
      .attr("class", "label")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text(ylabel);

  // draw dots
  svg.selectAll(".dot")
      .data(launch.data)
    .enter().append("circle")
      .attr("class", "dot")
      .attr("r", 3.5)
      .attr("cx", xMap)
      .attr("cy", yMap)
      .on("mouseover", function(d) {
          tooltip.transition()
               .duration(200)
               .style("opacity", .9);
          tooltip.html(d.time + "<br/> (" + xValue(d)
	        + ", " + yValue(d) + ")")
               .style("left", (d3.event.pageX + 5) + "px")
               .style("top", (d3.event.pageY - 28) + "px");
      })
      .on("mouseout", function(d) {
          tooltip.transition()
               .duration(500)
               .style("opacity", 0);
      });


      var print_btn = 'print-'+vid;

      $("#"+vid).append('<a class="print-btn" id="'+print_btn+'"> Print this graph only</a>');

      $("#"+print_btn).click(function() { print_svg(vid); });

}

function print_svg(svg_id)
{
  $('.svg-graph').each(function(){
      if($(this).attr('id') != svg_id){
        $(this).addClass('no-print');
      }
      else{
        $(this).removeClass('no-print');
      }
  });
  window.print();
}




function displayInfo(){
  //console.log(launch);
  $('#loading').css('display','none');
  $('#site-name').text("Launch: "+launch.title);
  $('#launch-date').text("Created: "+launch.created);
  for(var i=0; i < launch.info.length; i++){

    $('#info').append("<li><b>"+ launch.info[i].label+"</b> : "+ launch.info[i].value);
  }
}


      function getAPI(APIfile){
                console.log(APIfile);
                //console.log('https://api.figshare.com/v2/articles?search_for=Munday');
                jQuery.ajax({
                  //url: "https://api.figsh.com/v2/account/articles?access_token=de4df3fbbff9c50a1e4a5e547f1350b59360b303dc2ed183a99aa3362ceb2769338f61a51bdd2af64073d27e449c9062f10dc7c81a7b975bc752c2a26f3d33f7",
                  url:'https://api.figsh.com/v2/account/articles/'+APIfile+'?access_token=de4df3fbbff9c50a1e4a5e547f1350b59360b303dc2ed183a99aa3362ceb2769338f61a51bdd2af64073d27e449c9062f10dc7c81a7b975bc752c2a26f3d33f7',
                  success: function(result) {

                      console.log(result.files.length);
                      console.log(result);

                      for(var i=0; i < result.files.length; i++){

                        //var apiItem = '<h4 ><a href="/display/?id='+ result.files[i].id + '" id="'+ result.files[i].id + '" class="api-item" >';
                        var apiItem = '<h4 ><a href ="#'+ result.files[i].id + '" id="'+ result.files[i].id + '" class="api-item" >';
                        var str = result.files[i].name;
                        var n = str.indexOf("20");
                        var dateString = str.substring(n, (n+8) );

                        var year = dateString.substring(0,4);
                        var month = Number(dateString.substring(4,6)) - 1;
                        var day = dateString.substring(6,8);
                        //console.log( moment([year, month, day]).format("dddd, MMMM Do YYYY") );
                        apiItem = apiItem + moment([year, month, day]).format("dddd, MMMM Do YYYY");
                        apiItem = apiItem +'</a"></h4>';
                        //console.log(apiItem);


                        $('#parse').append(apiItem);
                        //getItemDetails(i, result[i].url)
                      }

                      $('.api-item').click(function (e) {
                        console.log( $(this).attr('id') );
                        getDatFile('https://api.figsh.com/v2/file/download/'+$(this).attr('id') );
                        $('#listing').css('display','none');
                        $('#launch').css('display','block');

                      });


                  },
                });
              }


            if ( $( "#api" ).length ) {

              getAPI( $( "#api" ).text() );

            }


            if ( $( "#dataFile" ).length ) {

                console.log($( "#dataFile" ).text());
                $( "#dataFile" ).css("display", "none");
                //getDatFile('http://localhost:8888/wp-content/uploads/2015/09/ftworth_2011060418_intexb.odt');
                getDatFile( $( "#dataFile" ).text() );

            }

            function getParameterByName(name) {
                name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                    results = regex.exec(location.search);
                return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
            }


            var dataId = getParameterByName('id');
            console.log(dataId);
            if(dataId != ''){
              //getDatFile( 'https://api.figsh.com/v2/file/download/'+dataId );
            }





// Helper method to parse the title tag from the response.
function getTitle(text) {
  return text.match('<title>(.*)?</title>')[1];
}





}); /* end of as page load scripts */





/*! A fix for the iOS orientationchange zoom bug.
 Script by @scottjehl, rebound by @wilto.
 MIT License.
*/
(function(w){
	// This fix addresses an iOS bug, so return early if the UA claims it's something else.
    'use strict';
	if( !( /iPhone|iPad|iPod/.test( navigator.platform ) && navigator.userAgent.indexOf( 'AppleWebKit' ) > -1 ) ){ return; }
    var doc = w.document;
    if( !doc.querySelector ){ return; }
    var meta = doc.querySelector( 'meta[name=viewport]' ),
        initialContent = meta && meta.getAttribute( 'content' ),
        disabledZoom = initialContent + ',maximum-scale=1',
        enabledZoom = initialContent + ',maximum-scale=10',
        enabled = true,
		x, y, z, aig;
    if( !meta ){ return; }
    function restoreZoom(){
        meta.setAttribute( 'content', enabledZoom );
        enabled = true; }
    function disableZoom(){
        meta.setAttribute( 'content', disabledZoom );
        enabled = false; }
    function checkTilt( e ){
		aig = e.accelerationIncludingGravity;
		x = Math.abs( aig.x );
		y = Math.abs( aig.y );
		z = Math.abs( aig.z );
		// If portrait orientation and in one of the danger zones
        if( !w.orientation && ( x > 7 || ( ( z > 6 && y < 8 || z < 8 && y > 6 ) && x > 5 ) ) ){
			if( enabled ){ disableZoom(); } }
		else if( !enabled ){ restoreZoom(); } }
	w.addEventListener( 'orientationchange', restoreZoom, false );
	w.addEventListener( 'devicemotion', checkTilt, false );
})( this );

/*
 * Load up Foundation
 */
(function(jQuery) {
  'use strict';
  jQuery(document).foundation();
})(jQuery);
