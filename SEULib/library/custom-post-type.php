<?php
/* joints Custom Post Type Example
This page walks you through creating 
a custom post type and taxonomies. You
can edit this one or copy the following code 
to create another one. 

I put this in a separate file so as to 
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

*/


// let's create the function for the custom type
function custom_post_example() { 
	// creating (registering) the custom type 
	register_post_type( 'gallery_post', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Gallery Posts', 'jointstheme'), /* This is the Title of the Group */
			'singular_name' => __('Gallery Post', 'jointstheme'), /* This is the individual type */
			'all_items' => __('All Gallery Posts', 'jointstheme'), /* the all items menu item */
			'add_new' => __('Add New Gallery Post', 'jointstheme'), /* The add new menu item */
			'add_new_item' => __('Add New Gallery Post', 'jointstheme'), /* Add New Display Title */
			'edit' => __( 'Edit', 'jointstheme' ), /* Edit Dialog */
			'edit_item' => __('Edit Gallery Posts', 'jointstheme'), /* Edit Display Title */
			'new_item' => __('New Gallery Post', 'jointstheme'), /* New Display Title */
			'view_item' => __('View Gallery Post', 'jointstheme'), /* View Display Title */
			'search_items' => __('Search Gallery Post', 'jointstheme'), /* Search Custom Type Title */ 
			'not_found' =>  __('Nothing found in the Database.', 'jointstheme'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash', 'jointstheme'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the example Gallery Post', 'jointstheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => get_stylesheet_directory_uri() . '/library/images/custom-post-icon.png', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'gallery_post', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'gallery_post', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
	 	) /* end of options */
	); /* end of register post type */
	
	/* this adds your post categories to your custom post type */
	//register_taxonomy_for_object_type('category', 'gallery_post');
	/* this adds your post tags to your custom post type */
	//register_taxonomy_for_object_type('post_tag', 'gallery_post');
	
} 

	// adding the function to the Wordpress init
	add_action( 'init', 'custom_post_example');
	
	/*
	for more information on taxonomies, go here:
	http://codex.wordpress.org/Function_Reference/register_taxonomy
	*/
	
	// now let's add custom categories (these act like categories)
    register_taxonomy( 'galleries_cat', 
    	array('gallery_post'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
    	array('hierarchical' => true,     /* if this is true, it acts like categories */             
    		'labels' => array(
    			'name' => __( 'Galleries', 'jointstheme' ), /* name of the custom taxonomy */
    			'singular_name' => __( 'Custom Gallery', 'jointstheme' ), /* single taxonomy name */
    			'search_items' =>  __( 'Search Galleries', 'jointstheme' ), /* search title for taxomony */
    			'all_items' => __( 'All Galleries', 'jointstheme' ), /* all title for taxonomies */
    			'parent_item' => __( 'Parent Gallery', 'jointstheme' ), /* parent title for taxonomy */
    			'parent_item_colon' => __( 'Parent Custom Category:', 'jointstheme' ), /* parent taxonomy title */
    			'edit_item' => __( 'Edit Gallery', 'jointstheme' ), /* edit custom taxonomy title */
    			'update_item' => __( 'Update Gallery', 'jointstheme' ), /* update title for taxonomy */
    			'add_new_item' => __( 'Add New Gallery', 'jointstheme' ), /* add new title for taxonomy */
    			'new_item_name' => __( 'New Custom Gallery Name', 'jointstheme' ) /* name title for taxonomy */
    		),
    		'show_admin_column' => true, 
    		'show_ui' => true,
    		'query_var' => true,
    		'rewrite' => array( 'slug' => 'gallery' ),
    	)
    );   
    
	
    
    /*
    	looking for custom meta boxes?
    	check out this fantastic tool:
    	https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress
    */

    /*
Plugin Name: Demo Tax meta class
Plugin URI: http://en.bainternet.info
Description: Tax meta class usage demo
Version: 2.0.2
Author: Bainternet, Ohad Raz
Author URI: http://en.bainternet.info
*/

//include the main class file
require_once("Tax-meta-class/Tax-meta-class.php");
if (is_admin()){
  /* 
   * prefix of meta keys, optional
   */
  $prefix = 'ba_';
  /* 
   * configure your meta box
   */
  $config = array(
    'id' => 'demo_meta_box',          // meta box id, unique per meta box
    'title' => 'Demo Meta Box',          // meta box title
    'pages' => array('galleries_cat'),        // taxonomy name, accept categories, post_tag and custom taxonomies
    'context' => 'normal',            // where the meta box appear: normal (default), advanced, side; optional
    'fields' => array(),            // list of meta fields (can be added by field arrays)
    'local_images' => false,          // Use local or hosted images (meta box images for add/remove)
    'use_with_theme' => false          //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
  );
  
  
  /*
   * Initiate your meta box
   */
  $my_meta =  new Tax_Meta_Class($config);
  
  /*
   * Add fields to your meta box
   */
  
  //text field
  		$my_meta->addText($prefix.'semester_field_id',array('name'=> __('Semester/Year ','tax-meta'),'desc' => 'Ex. Fall 2014'));
  		$my_meta->addText($prefix.'student_field_id',array('name'=> __('Student/Photographer ','tax-meta'),'desc' => 'Enter Student name'));
 	  	

 	  	$my_meta->addText($prefix.'student_email_field_id',array('name'=> __('Student Email ','tax-meta'),'desc' => ''));
 	  	$my_meta->addText($prefix.'student_portfolio_field_id',array('name'=> __('Student Portfolio ','tax-meta'),'desc' => ''));
 
  /*
   * To Create a reapeater Block first create an array of fields
   * use the same functions as above but add true as a last param
   */
  
  //$repeater_fields[] = $my_meta->addText($prefix.'student_field_id',array('name'=> __('Student/Photographer ','tax-meta')),true);

  
  /*
   * Then just add the fields to the repeater block
   */
  //repeater block
  //$my_meta->addRepeaterBlock($prefix.'re_',array('inline' => true, 'name' => __('This is a Repeater Block','tax-meta'),'fields' => $repeater_fields));
  /*
   * Don't Forget to Close up the meta box decleration
   */
  //Finish Meta Box Decleration
  $my_meta->Finish();
}

?>