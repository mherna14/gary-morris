<?php
/* joints Custom Post Type Example
This page walks you through creating
a custom post type and taxonomies. You
can edit this one or copy the following code
to create another one.

I put this in a separate file so as to
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

*/


// let's create the function for the custom type
function custom_post_site() {
	// creating (registering) the custom type
	register_post_type( 'site', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Sites', 'jointstheme'), /* This is the Title of the Group */
			'singular_name' => __('Site', 'jointstheme'), /* This is the individual type */
			'all_items' => __('All Sites', 'jointstheme'), /* the all items menu item */
			'add_new' => __('Add New Site', 'jointstheme'), /* The add new menu item */
			'add_new_item' => __('Add New Site', 'jointstheme'), /* Add New Display Title */
			'edit' => __( 'Edit', 'jointstheme' ), /* Edit Dialog */
			'edit_item' => __('Edit Site', 'jointstheme'), /* Edit Display Title */
			'new_item' => __('New Site', 'jointstheme'), /* New Display Title */
			'view_item' => __('View Sites', 'jointstheme'), /* View Display Title */
			'search_items' => __('Search Site', 'jointstheme'), /* Search Custom Type Title */
			'not_found' =>  __('Nothing found in the Database.', 'jointstheme'), /* This displays if there are no entries yet */
			'not_found_in_trash' => __('Nothing found in Trash', 'jointstheme'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the example custom post type', 'jointstheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 1, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' => get_stylesheet_directory_uri() . '/library/img/custom-post-icon.png', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'site', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'site', /* you can rename the slug here */
			'capability_type' => 'page',
			'hierarchical' => true,
			/* the next one is important, it tells what's enabled in the post editor */
			//'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'page-attributes')
			'supports' => array('title', 'editor', 'author', 'page-attributes', 'thumbnail', 'excerpt', 'custom-fields', 'revisions')
	 	) /* end of options */
	); /* end of register post type */

	/* this adds your post categories to your custom post type */
	//register_taxonomy_for_object_type('category', 'site');
	/* this adds your post tags to your custom post type */
	//register_taxonomy_for_object_type('post_tag', 'site');

}

	// adding the function to the Wordpress init
	add_action( 'init', 'custom_post_site');

	/**
	 * Include and setup custom metaboxes and fields.
	 *
	 * @category YourThemeOrPlugin
	 * @package  Metaboxes
	 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
	 * @link     https://github.com/webdevstudios/Custom-Metaboxes-and-Fields-for-WordPress
	 */

	add_filter( 'cmb_meta_boxes', 'cmb_site_metaboxes' );
	/**
	 * Define the metabox and field configurations.
	 *
	 * @param  array $meta_boxes
	 * @return array
	 */
	function cmb_site_metaboxes( array $meta_boxes ) {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_cmb_';

		/**
		 * Sample metabox to demonstrate each field type included
		 */
		$meta_boxes['site_metabox'] = array(
			'id'         => 'site_metabox',
			'title'      => __( 'Figshare Info', 'cmb' ),
			'pages'      => array( 'site', ), // Post type
			'context'    => 'normal',
			'priority'   => 'high',
			'show_names' => true, // Show field names on the left
			// 'cmb_styles' => true, // Enqueue the CMB stylesheet on the frontend
			'fields'     => array(
				array(
					'name'       => __( 'Figshare item ID', 'cmb' ),
					'desc'       => __( 'Figshare item ID', 'cmb' ),
					'id'         => $prefix . 'figsh-url',
					'type'       => 'text',
					'show_on_cb' => 'cmb_test_text_show_on_cb', // function should return a bool value
					// 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
					// 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
					// 'on_front'        => false, // Optionally designate a field to wp-admin only
					// 'repeatable'      => true,
				)
				/*
				array(
					'name' => __( 'Launch Data file', 'cmb' ),
					'desc' => __( 'Upload an image or enter a URL.', 'cmb' ),
					'id'   => $prefix . 'data_file',
					'type' => 'file',
				)

				//_cmb_data_file

				array(
					'name' => __( 'Test Text Small', 'cmb' ),
					'desc' => __( 'field description (optional)', 'cmb' ),
					'id'   => $prefix . 'test_textsmall',
					'type' => 'text_small',
					// 'repeatable' => true,
				),
				array(
					'name' => __( 'Test Text Medium', 'cmb' ),
					'desc' => __( 'field description (optional)', 'cmb' ),
					'id'   => $prefix . 'test_textmedium',
					'type' => 'text_medium',
					// 'repeatable' => true,
				),
				array(
					'name' => __( 'Website URL', 'cmb' ),
					'desc' => __( 'field description (optional)', 'cmb' ),
					'id'   => $prefix . 'url',
					'type' => 'text_url',
					// 'protocols' => array('http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet'), // Array of allowed protocols
					// 'repeatable' => true,
				),
				array(
					'name' => __( 'Test Text Email', 'cmb' ),
					'desc' => __( 'field description (optional)', 'cmb' ),
					'id'   => $prefix . 'email',
					'type' => 'text_email',
					// 'repeatable' => true,
				),
				array(
					'name' => __( 'Test Time', 'cmb' ),
					'desc' => __( 'field description (optional)', 'cmb' ),
					'id'   => $prefix . 'test_time',
					'type' => 'text_time',
				),
				array(
					'name' => __( 'Time zone', 'cmb' ),
					'desc' => __( 'Time zone', 'cmb' ),
					'id'   => $prefix . 'timezone',
					'type' => 'select_timezone',
				),
				array(
					'name' => __( 'Test Date Picker', 'cmb' ),
					'desc' => __( 'field description (optional)', 'cmb' ),
					'id'   => $prefix . 'test_textdate',
					'type' => 'text_date',
				),
				array(
					'name' => __( 'Test Date Picker (UNIX timestamp)', 'cmb' ),
					'desc' => __( 'field description (optional)', 'cmb' ),
					'id'   => $prefix . 'test_textdate_timestamp',
					'type' => 'text_date_timestamp',
					// 'timezone_meta_key' => $prefix . 'timezone', // Optionally make this field honor the timezone selected in the select_timezone specified above
				),
				array(
					'name' => __( 'Test Date/Time Picker Combo (UNIX timestamp)', 'cmb' ),
					'desc' => __( 'field description (optional)', 'cmb' ),
					'id'   => $prefix . 'test_datetime_timestamp',
					'type' => 'text_datetime_timestamp',
				),
				// This text_datetime_timestamp_timezone field type
				// is only compatible with PHP versions 5.3 or above.
				// Feel free to uncomment and use if your server meets the requirement
				// array(
				// 	'name' => __( 'Test Date/Time Picker/Time zone Combo (serialized DateTime object)', 'cmb' ),
				// 	'desc' => __( 'field description (optional)', 'cmb' ),
				// 	'id'   => $prefix . 'test_datetime_timestamp_timezone',
				// 	'type' => 'text_datetime_timestamp_timezone',
				// ),
				array(
					'name' => __( 'Test Money', 'cmb' ),
					'desc' => __( 'field description (optional)', 'cmb' ),
					'id'   => $prefix . 'test_textmoney',
					'type' => 'text_money',
					// 'before'     => '£', // override '$' symbol if needed
					// 'repeatable' => true,
				),
				array(
					'name'    => __( 'Test Color Picker', 'cmb' ),
					'desc'    => __( 'field description (optional)', 'cmb' ),
					'id'      => $prefix . 'test_colorpicker',
					'type'    => 'colorpicker',
					'default' => '#ffffff'
				),
				array(
					'name' => __( 'Test Text Area', 'cmb' ),
					'desc' => __( 'field description (optional)', 'cmb' ),
					'id'   => $prefix . 'test_textarea',
					'type' => 'textarea',
				),
				array(
					'name' => __( 'Test Text Area Small', 'cmb' ),
					'desc' => __( 'field description (optional)', 'cmb' ),
					'id'   => $prefix . 'test_textareasmall',
					'type' => 'textarea_small',
				),
				array(
					'name' => __( 'Test Text Area for Code', 'cmb' ),
					'desc' => __( 'field description (optional)', 'cmb' ),
					'id'   => $prefix . 'test_textarea_code',
					'type' => 'textarea_code',
				),
				array(
					'name' => __( 'Test Title Weeeee', 'cmb' ),
					'desc' => __( 'This is a title description', 'cmb' ),
					'id'   => $prefix . 'test_title',
					'type' => 'title',
				),
				array(
					'name'    => __( 'Test Select', 'cmb' ),
					'desc'    => __( 'field description (optional)', 'cmb' ),
					'id'      => $prefix . 'test_select',
					'type'    => 'select',
					'options' => array(
						'standard' => __( 'Option One', 'cmb' ),
						'custom'   => __( 'Option Two', 'cmb' ),
						'none'     => __( 'Option Three', 'cmb' ),
					),
				),
				array(
					'name'    => __( 'Test Radio inline', 'cmb' ),
					'desc'    => __( 'field description (optional)', 'cmb' ),
					'id'      => $prefix . 'test_radio_inline',
					'type'    => 'radio_inline',
					'options' => array(
						'standard' => __( 'Option One', 'cmb' ),
						'custom'   => __( 'Option Two', 'cmb' ),
						'none'     => __( 'Option Three', 'cmb' ),
					),
				),
				array(
					'name'    => __( 'Test Radio', 'cmb' ),
					'desc'    => __( 'field description (optional)', 'cmb' ),
					'id'      => $prefix . 'test_radio',
					'type'    => 'radio',
					'options' => array(
						'option1' => __( 'Option One', 'cmb' ),
						'option2' => __( 'Option Two', 'cmb' ),
						'option3' => __( 'Option Three', 'cmb' ),
					),
				),
				array(
					'name'     => __( 'Test Taxonomy Radio', 'cmb' ),
					'desc'     => __( 'field description (optional)', 'cmb' ),
					'id'       => $prefix . 'text_taxonomy_radio',
					'type'     => 'taxonomy_radio',
					'taxonomy' => 'category', // Taxonomy Slug
					// 'inline'  => true, // Toggles display to inline
				),
				array(
					'name'     => __( 'Test Taxonomy Select', 'cmb' ),
					'desc'     => __( 'field description (optional)', 'cmb' ),
					'id'       => $prefix . 'text_taxonomy_select',
					'type'     => 'taxonomy_select',
					'taxonomy' => 'category', // Taxonomy Slug
				),
				array(
					'name'     => __( 'Test Taxonomy Multi Checkbox', 'cmb' ),
					'desc'     => __( 'field description (optional)', 'cmb' ),
					'id'       => $prefix . 'test_multitaxonomy',
					'type'     => 'taxonomy_multicheck',
					'taxonomy' => 'post_tag', // Taxonomy Slug
					// 'inline'  => true, // Toggles display to inline
				),
				array(
					'name' => __( 'Test Checkbox', 'cmb' ),
					'desc' => __( 'field description (optional)', 'cmb' ),
					'id'   => $prefix . 'test_checkbox',
					'type' => 'checkbox',
				),
				array(
					'name'    => __( 'Test Multi Checkbox', 'cmb' ),
					'desc'    => __( 'field description (optional)', 'cmb' ),
					'id'      => $prefix . 'test_multicheckbox',
					'type'    => 'multicheck',
					'options' => array(
						'check1' => __( 'Check One', 'cmb' ),
						'check2' => __( 'Check Two', 'cmb' ),
						'check3' => __( 'Check Three', 'cmb' ),
					),
					// 'inline'  => true, // Toggles display to inline
				),
				array(
					'name'    => __( 'Test wysiwyg', 'cmb' ),
					'desc'    => __( 'field description (optional)', 'cmb' ),
					'id'      => $prefix . 'test_wysiwyg',
					'type'    => 'wysiwyg',
					'options' => array( 'textarea_rows' => 5, ),
				),
				array(
					'name' => __( 'Test Image', 'cmb' ),
					'desc' => __( 'Upload an image or enter a URL.', 'cmb' ),
					'id'   => $prefix . 'test_image',
					'type' => 'file',
				),
				array(
					'name'         => __( 'Multiple Files', 'cmb' ),
					'desc'         => __( 'Upload or add multiple images/attachments.', 'cmb' ),
					'id'           => $prefix . 'test_file_list',
					'type'         => 'file_list',
					'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
				),
				array(
					'name' => __( 'oEmbed', 'cmb' ),
					'desc' => __( 'Enter a youtube, twitter, or instagram URL. Supports services listed at <a href="http://codex.wordpress.org/Embeds">http://codex.wordpress.org/Embeds</a>.', 'cmb' ),
					'id'   => $prefix . 'test_embed',
					'type' => 'oembed',
				),
				*/
			),
		);





		// Add other metaboxes as needed

		return $meta_boxes;
	}

	add_action( 'init', 'cmb_meta_boxes', 9999 );
	/**
	 * Initialize the metabox class.
	 */
	function cmb_meta_boxes() {

		if ( ! class_exists( 'cmb_Meta_Box' ) )
			require_once 'CMB/init.php';

	}


?>
