<?php
/* joints Custom Post Type Example
This page walks you through creating
a custom post type and taxonomies. You
can edit this one or copy the following code
to create another one.

I put this in a separate file so as to
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

*/


// let's create the function for the custom type
function custom_post_help_link() {
	// creating (registering) the custom type
	register_post_type( 'help_link', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Help  links', 'jointstheme'), /* This is the Title of the Group */
			'singular_name' => __('Help  Links', 'jointstheme'), /* This is the individual type */
			'all_items' => __('All Help  Links', 'jointstheme'), /* the all items menu item */
			'add_new' => __('Add New Help  Links', 'jointstheme'), /* The add new menu item */
			'add_new_item' => __('Add New Help  Links', 'jointstheme'), /* Add New Display Title */
			'edit' => __( 'Edit', 'jointstheme' ), /* Edit Dialog */
			'edit_item' => __('Edit Help  Links', 'jointstheme'), /* Edit Display Title */
			'new_item' => __('New Help  Links', 'jointstheme'), /* New Display Title */
			'view_item' => __('View Help  Linkss', 'jointstheme'), /* View Display Title */
			'search_items' => __('Search Help  Links', 'jointstheme'), /* Search Custom Type Title */
			'not_found' =>  __('Nothing found in the Help .', 'jointstheme'), /* This displays if there are no entries yet */
			'not_found_in_trash' => __('Nothing found in Trash', 'jointstheme'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the example custom post type', 'jointstheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' => get_stylesheet_directory_uri() . '/library/img/custom-post-icon.png', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'help_link', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'Help s', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'excerpt')
	 	) /* end of options */
	); /* end of register post type */

	/* this adds your post categories to your custom post type */
	register_taxonomy_for_object_type('category', 'help_link');
	/* this adds your post tags to your custom post type */
	register_taxonomy_for_object_type('post_tag', 'help_link');

}

	// adding the function to the Wordpress init
	add_action( 'init', 'custom_post_help_link');

	/*
	for more information on taxonomies, go here:
	http://codex.wordpress.org/Function_Reference/register_taxonomy
	*/




    /*
    	looking for custom meta boxes?
    	check out this fantastic tool:
    	https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress
    */

    add_filter( 'cmb_meta_boxes', 'help_links_metaboxes' );
	/**
	 * Define the metabox and field configurations.
	 *
	 * @param  array $meta_boxes
	 * @return array
	 */
	function help_links_metaboxes( array $meta_boxes ) {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_cmb_';

		add_action( 'init', 'cmb_initialize_cmb_meta_boxes', 9999 );

		$meta_boxes['help_metabox'] = array(
		'id'         => 'help_metabox',
		'title'      => __( 'Help  Link', 'cmb' ),
		'pages'      => array( 'help_link', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		// 'cmb_styles' => true, // Enqueue the CMB stylesheet on the frontend
		'fields'     => array(

			array(
					'name' => __( 'Help link  URL', 'cmb' ),
					'desc' => __( 'field description (optional)', 'cmb' ),
					'id'   => $prefix . 'help_url',
					'type' => 'text_url',
					// 'protocols' => array('http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet'), // Array of allowed protocols
					// 'repeatable' => true,
				)
			),
		);


		return $meta_boxes;
	}

add_action( 'init', 'help_link_initialize_cmb_meta_boxes', 9999 );

/**
 * Initialize the metabox class.
 */
function help_link_initialize_cmb_meta_boxes() {

	if ( ! class_exists( 'cmb_Meta_Box' ) )
		require_once 'CMB/init.php';

}


?>
