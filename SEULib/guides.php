<?php
/*
Template Name: Guides Page
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div  class="row clearfix">
					<div class="large-12 columns">
						<?php the_breadcrumb(); ?>
					</div>
				</div>

				<div id="inner-content" class="row clearfix">

					<aside class="large-3 medium-3 columns clearfix">
						<ul class="nav nav-pills nav-stacked">
							<?php
							 $args = array(
								'title_li'           => __( '<span class="subject-title">Subjects/Tags</span>' )
							    );

							wp_list_categories( $args ); ?>
						</ul>
					</aside>


				    <main id="recent-guides-list" class="large-9 medium-9 columns clearfix" role="main">



				    	<?php $loop = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => '-1', 'order'=> 'DESC') ); ?>

					    <ul class="small-block-grid-2">

						    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

								<li><?php get_template_part( 'partials/loop', 'archive' ); ?></li>

						    <?php endwhile; ?>

						</ul>








				    </main> <!-- end #main -->



				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>
