
<?php get_header(); ?>

			<div id="content">

				<div  class="row clearfix">
					<div class="large-12 columns">
						<?php the_breadcrumb(); ?>
					</div>
				</div>

				<div id="inner-content" class="row clearfix">

				    <main id="main" class="large-9 medium-push-3 medium-9 columns" role="main">

					    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					    	<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">



							    <section class="entry-content clearfix" itemprop="articleBody">
									<?php the_post_thumbnail('large'); ?>
									
									<?php the_content(); ?>
								</section> <!-- end article section -->

								<?php get_template_part( 'partials/content', 'books' ); ?>
								<?php get_template_part( 'partials/content', 'db_links' ); ?>

								<footer class="article-footer">
									<p class="tags"><?php the_tags('<span class="tags-title">' . __('Tags:', 'jointstheme') . '</span> ', ', ', ''); ?></p>	</footer> <!-- end article footer -->



							</article> <!-- end article -->








					    <?php endwhile; else : ?>

					   		<?php get_template_part( 'partials/content', 'missing' ); ?>

					    <?php endif; ?>

    				</main > <!-- end #main -->

				    <?php get_sidebar('page'); ?>

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>
