<?php get_header(); ?>
			
			<div id="content">
			
				<div id="inner-content" class="row clearfix">


			
				    <main id="db_links" class="large-8 medium-8 columns clearfix" role="main">

				    	<div class="row">
				    		<div class="large-6 medium-6 columns">
				    			<?php 
				    			

				    			?>
				    			<select name="event-dropdown" onchange='document.location.href=this.options[this.selectedIndex].value;'> 
								    <option value="">Select Category</option> 

								    <?php 
								     
								        $categories = get_categories(); 
								        foreach ($categories as $category) {
								        	$isSelected = (single_cat_title("", false) === $category->cat_name ? 'selected':'');

								            $option .= '<option value="'. home_url().'/category/'.$category->slug.'" '.$isSelected.'>';
								            $option .= $category->cat_name;
								            $option .= ' ('.$category->category_count.')';
								            $option .= '</option>';
								        }
								        echo $option;
								    ?>
								</select>
				    		</div>
				    		<div class="large-6 medium-6 columns">
				    			<?php 

									$db_args = array(
										'type' => 'db_link'
									); 

								?>
								<select name="event-dropdown" onchange='document.location.href=this.options[this.selectedIndex].value;'> 
								    <option value="">Select Vendor</option> 

								    <?php 
								       
								        $terms = get_terms('db_vendor','hide-empty=0&orderby=id'); 
								        foreach ($terms as $term) {
								            $vendor_option .= '<option value="'. home_url().'/vendor/'.$term->slug.'">';
								            $vendor_option .= $term->name;
								       		$vendor_option .= ' ('.$term->count.')';
								            $vendor_option .= '</option>';
								        }
								        echo $vendor_option;
								    ?>
								</select>
				    			
				    		</div>
				    	</div>


				    	<?php 
				    	$letterArray = array();
				    	$selectedLetter = '';
				    	
				    	if(isset($_GET['letter'])){
				    		$selectedLetter = $_GET['letter']; ?>
				    		<h3 class="letter-seperate"><?php echo $selectedLetter; ?></h3>
				    	<?php }
				    	$currentLetter = '';

				    	?>
				    	<?php $loop = new WP_Query( array( 'post_type' => 'db_link', 'posts_per_page' => '-1', 'order'=> 'ASC', 'category_name' => single_cat_title('', false)) ); ?>
					    
					    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
					    

					    	<?php


								$string = get_the_title();
								$firstLetter = substr($string, 0, 1);
								
								if( $currentLetter != $firstLetter){ 
									array_push($letterArray,$firstLetter);
									if($selectedLetter === ''){
									?>
										<h3 id="<?php echo $firstLetter; ?>" class="letter-seperate"><?php echo $firstLetter; ?></h3>
								<?php
									}
									$currentLetter = $firstLetter;
								}
								
							?>
							<?php if ($selectedLetter === '' or $selectedLetter === $currentLetter ){ 
								
								?>

					
						    	<?php get_template_part( 'partials/loop', 'db_link' ); ?>
							<?php } ?>
					
					    <?php endwhile; ?>	

					    <div id="az-menu">

						    <ul id="alphabet-menu">
						    	<li class="az-char"><a href="/databases/">All files</a></li>
						    <?php

						    foreach(range('a', 'z') as $i) :
						    	$letterFind = array_search(strtoupper($i), $letterArray);
						    	if($letterFind > -1){ ?>
						    		<li class="az-char"><a href="?letter=<?php echo strtoupper($i); ?>"><?php echo strtoupper($i); ?></a></li>
						    	<?php
						    	}else{ ?>
						    		<li class="az-char"><?php echo strtoupper($i); ?></li>
						    	<?php }

						    endforeach;

						    ?>
						    </ul>

						</div>

					   
					
					

			
				    </main> <!-- end #main -->
    
				    <?php get_sidebar('db_links'); ?>
				    
				</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>

