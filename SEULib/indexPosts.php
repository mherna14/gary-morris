<?php get_header(); ?>

			<div id="content" role="main">
				<div  class="row clearfix">
					<div class="large-12 columns">
						<?php the_breadcrumb(); ?>
					</div>
				</div>
				<div id="inner-content" class="row clearfix">

					<div id="recent-guides-list" class="large-6 columns clearfix" >
						<h3>Recent Guides</h3>
						 <?php
					    if (have_posts()) : while (have_posts()) : the_post(); ?>

					    	<?php get_template_part( 'partials/loop', 'archive' ); ?>

					    <?php endwhile; endif; ?>

						<a href="/guides/" class="button radius small secondary">More Guides</a>

					</div>



				    <div class="large-3 columns clearfix" >

						  <?php

						  	$excludeList = array();

						  	$getProfilesPage = get_page_by_path('profiles');
						    if ($getProfilesPage) {
						        //$getProfilesPageID =  $getProfilesPage->ID;
						        array_push($excludeList, $getProfilesPage->ID);
						    }

						    $getGuidesPage = get_page_by_path('guides');
						    if ($getGuidesPage) {
						        //$getProfilesPageID =  $getProfilesPage->ID;
						        array_push($excludeList, $getGuidesPage->ID);
						    }

							$pages = get_pages('parent=0&sort_order=ASC&exclude='.implode(",",$excludeList)); // Get published parent pages only
							if($pages  > 0){
								?>

								<h3>Showing <?php echo count( $pages );?> Guides</h3>
								<ul id="menu-homepage">
								<?php


								foreach($pages as $page){
									$user_info = get_userdata($page->post_author); // Get Author's info
								    $username = $user_info->user_login;
								    $first_name = $user_info->first_name;
								    $last_name = $user_info->last_name;

									if($page->post_excerpt){
										$excerpt = $page->post_excerpt;
									}
									else{
										$excerpt = $page->post_title;
									}
								    ?>

									<li>
										<h4><a href="<?php echo  $page->post_name; ?>" ><?php echo $page->post_title ?></a></h4>
										<span class="info"> <?php echo $excerpt; ?></span>

									</li>

									<?php
								}

								 ?>
								</ul>
							<?php
							}

						?>


				    	<?php
				    	/*
				    	 if ( is_active_sidebar( 'homepage' ) ) : ?>

							<?php dynamic_sidebar( 'homepage' ); ?>

						<?php else : ?>

						<!-- This content shows up if there are no widgets defined in the backend. -->

							<div class="alert help">
								<p><?php _e("Please add published guides to list.", "jointstheme");  ?></p>
							</div>

						<?php endif;
						*/
						?>



					    <?php
					    /*
					    if (have_posts()) : while (have_posts()) : the_post(); ?>

					    	<?php get_template_part( 'partials/loop', 'archive' ); ?>

					    <?php endwhile; ?>

					        <?php if (function_exists('joints_page_navi')) { ?>
					            <?php joints_page_navi(); ?>
					        <?php } else { ?>

					            <nav class="wp-prev-next">
					                <ul class="clearfix">
					        	        <li class="prev-link"><?php next_posts_link(__('&laquo; Older Entries', "jointstheme")) ?></li>
					        	        <li class="next-link"><?php previous_posts_link(__('Newer Entries &raquo;', "jointstheme")) ?></li>
					                </ul>
					            </nav>
					        <?php } ?>

					    <?php else : ?>

    						<?php get_template_part( 'partials/content', 'missing' ); ?>

					    <?php endif;

						*/
					    ?>




				    </div> <!-- end #main -->

				    <?php get_sidebar(); ?>

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>
