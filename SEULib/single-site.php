
<?php get_header(); ?>

			<div id="content">

				<div  class="row clearfix">
					<div class="large-12 columns">
						<?php the_breadcrumb(); ?>
					</div>
				</div>

				<div id="inner-content" class="row clearfix">

				    <main id="main" class="large-9 medium-push-3 medium-9 columns" role="main">

							<div id="listing">


							<h2><?php the_title(); ?></h2>

					    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>




										<?php

										$level = count(get_post_ancestors( get_the_ID() ));
										//echo $level;

										$figsh_url = get_post_meta( get_the_ID(), '_cmb_figsh-url' );
										if($figsh_url){ ?>
											<span id="api" style="display:none">	<?php echo $figsh_url[0];?></span>

										<?php }else {


												$years_arg=array(
													'post_parent' => get_the_ID()
												);
												$years = get_children($years_arg);
												if($years){
													if($level < 1){?>
														<h3>Sites</h3>
													<?php }else {?>
														<h3>Years</h3>
													<?php
													}
													foreach ($years as $year) { ?>
														<h4><a href="<?php echo  $year->post_name; ?>" ><?php echo $year->post_title ?></a></h4>

													<?php	}

												}

											}?>




					    	<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">



							    <section class="entry-content clearfix" itemprop="articleBody">
										<?php the_content(); ?>
										<div id="parse"></div>



									</section> <!-- end article section -->






							</article> <!-- end article -->

						</div>
						<div id="launch" style="display:none">
							<header>
								<h2 id="site-name"></h2>
								<h3 id="launch-date"></h3>
								<ul id="info"></ul>
							</header>
							<div id="visuals">
								<div id="loading">Loading...</div>
							</div>
							<form id="svgform" method="post" action="<?php echo get_template_directory_uri(); ?>/library/download.pl">
							 <input type="hidden" id="output_format" name="output_format" value="">
							 <input type="hidden" id="data" name="data" value="">
							</form>


						</div>


					    <?php endwhile; else : ?>

					   		<?php get_template_part( 'partials/content', 'missing' ); ?>

					    <?php endif; ?>

    				</main > <!-- end #main -->

				    <?php get_sidebar('page'); ?>

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>
