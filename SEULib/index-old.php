<?php get_header(); ?>

			<div id="content" role="main">
				<div  class="row clearfix">
					<div class="large-12 columns">
						<?php the_breadcrumb(); ?>

					</div>
				</div>
				<div id="inner-content" class="row clearfix">



				    <div class="large-8 columns clearfix" >


								<h3>US. Data</h3>
								<ul class="menu-homepage">
								<?php

								$args=array(
								  'post_type' => 'site',
								  'post_status' => 'publish',
								  'posts_per_page' => -1);

								$my_query = null;
								$my_query = new WP_Query($args);
								if( $my_query->have_posts() ) {
								  while ($my_query->have_posts()) : $my_query->the_post();


									?>

										<li>
											<span class="accordion-plus"></span>
											<h4><a href="<?php the_permalink() ?>" ><?php echo the_title(); ?></a></h4>

											<span class="info"> <?php the_excerpt(); ?></span>

										</li>


										<?php
								  endwhile;
								}
								wp_reset_query();  // Restore global post data stomped by the_post().

								 ?>
								</ul>









				    </div> <!-- end #main -->

				    <?php get_sidebar(); ?>

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>
