<div id="sidebar" class="sidebar large-4 medium-4 columns" role="complementary">


	<?php
	function get_top_parent_page_id() { //

			global $post;
			$ancestors = $post->ancestors;
			// Check if page is a child page (any level)
			if ($ancestors) {
					//  Grab the ID of top-level page from the tree
					return end($ancestors);
			} else {
					// Page is the top level, so use  it's own id
					return $post->ID;
			}
	}

		// Page/Research Guide heirarchy menu



		if( is_page() ){
					echo '<ul  class="menu-homepage">';
					$parent = get_top_parent_page_id($post->ID);
					// use wp_list_pages to display parent and all child pages all generations (a tree with parent)

					$args=array(
						'child_of' => $parent
					);
					$pages = get_pages($args);
					if ($pages) {
						$pageids = array();
						foreach ($pages as $page) {
							$pageids[]= $page->ID;
						}

						$args=array(
							'title_li' => '',
							'include' =>  $parent . ',' . implode(",", $pageids)
						);
						wp_list_pages($args);
					}
					echo '</ul>';
		}
		else if( is_single() ){

				echo get_the_category_list();
		}




	?>


	<?php if ( is_active_sidebar( 'sidebar' ) ) : ?>

		<?php dynamic_sidebar( 'sidebar' ); ?>

	<?php else : ?>

	<!-- This content shows up if there are no widgets defined in the backend. -->



	<?php endif; ?>


</div>
